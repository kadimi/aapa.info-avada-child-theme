<?php

function kds_option( $id ) {
	$options = array(
		'events_page_slug' => 'events',
		'links_page_slug' => 'links',
	);
	return $options[ $id ];
}

// http://stackoverflow.com/a/3835653
function str_last_replace( $search, $replace, $subject ) {
	$pos = strrpos( $subject, $search );
	if( $pos !== false ) {
		$subject = substr_replace( $subject, $replace, $pos, strlen( $search ) );
	}
	return $subject;
}
