<?php

// Events page content
add_filter( 'the_content', 'kds_events_content' );
function kds_events_content( $content ) {
	if ( trim( strip_tags( $content ) ) === '[avada_events]' ) {
		$style = '<style>.fusion-meta-info {margin-bottom: 60px; } </style>';
		$static_text = '';
		$term_slug = isset ( $_GET[ 'c' ] ) ? $_GET[ 'c' ] : '';
		$term = $term_slug
			? get_term_by( 'slug', $term_slug, 'events_category' )
			: null
		;
		$term_param = $term
			? 'cus_terms="events_category__' . $term_slug . '"'
			: ''
		;
		$title = $term
			? do_shortcode( '[title]' . $term->name . '[/title]')
			: ''
		;
		$events = do_shortcode( '[blog show_title="yes" title_link="yes" thumbnail="yes" excerpt="yes" excerpt_length="999" meta_all="yes" meta_author="no" meta_comments="no" meta_date="no" meta_link="yes" paging="yes" scrolling="pagination" strip_html="no" layout="medium" blog_grid_column_spacing="40" post_type="avada_events" cus_taxonomy="avada_events__events_category" ' . $term_param . '][/blog]' );


		$terms = $terms = get_terms( array(
			'taxonomy' => 'events_category',
		) );
		$terms_nav = sprintf( '<ul class="fusion-filters clearfix" style="display:block"><li class="fusion-filter fusion-filter-all %s"><a href="%s">All</a></li>'
			, ( $term ? '' : 'fusion-active' )
			, site_url( kds_option( 'events_page_slug' ) )
		);
		foreach ( $terms as $t ) {
			$terms_nav .= sprintf('<li class="fusion-filter %s"><a href="%s">%s</a></li>'			
				, ( $term_slug && $t->slug === $term_slug ? 'fusion-active' : '' )
				, site_url( kds_option( 'events_page_slug' ) . '/?c=' . $t->slug )
				, $t->name
			);  
		}
		$terms_nav .= '</ul>';

		$content = $style . $static_text . $terms_nav . $title . $events;
	}
	return $content;
}

// Events page content / Category
add_filter( 'term_link', 'kds_events_category_link_override', 10, 3 );
function kds_events_category_link_override( $url, $term, $taxonomy ) {
	if ( 'events_category' === $taxonomy ) {
		$url = site_url( kds_option( 'events_page_slug' ) . '/?c=' . $term->slug );
	}
	return $url;
}

// Redirect to event link
add_action( 'template_redirect', 'kds_events_CPT_redirect' );
function kds_events_CPT_redirect() {
	global $post;
	$queried_post_type = get_query_var('post_type');
	if ( is_single() && 'avada_events' ==  $queried_post_type ) {
		$url = get_post_meta($post->ID, 'web_link', true);
		if ( ! strpos( $url, '://') ) {
			$url = "http://$url";
		}
		wp_redirect( $url, 301 );
		exit;
	}
}

// Event content
add_filter('the_content', 'kds_event_content');
function kds_event_content( $content ) {
	global $post;
	$post_type = get_post_type( $post );
	switch( $post_type ) {
	case 'avada_events':
		$url = get_post_meta($post->ID, 'web_link', true);
		$start_date = get_post_meta($post->ID, 'start_date', true);
		$end_date = get_post_meta($post->ID, 'end_date', true);
		$date = "$start_date to $end_date";
		$url_light = str_replace( array( 'www.', 'http://', 'https://' ), '', $url );
		if ( ! strpos( $url, '://') ) {
			$url = "http://$url";
		}
		$link = sprintf( '<a href="%s">%s</a>', $url, $url_light );
		$categories = get_the_term_list( $post->ID, 'events_category', null, ', ' );
		$content = "<p>$categories | <strong>$date</strong></p>" . str_last_replace( '</p>', " &ndash; <em>$link</em></p>", $content );
		break;
	default:
		; 
	}
	return $content;
}
