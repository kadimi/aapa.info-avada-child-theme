<?php 

// Links page content
add_filter( 'the_content', 'kds_links_content' );
function kds_links_content( $content ) {
	if ( trim( strip_tags( $content ) ) === '[avada_links]' ) {
		$static_text = '<p>Members receive a complimentary listing – If you would like your organization listed, please send write-up and include which category you would like to be listed under. webmaster@aapa.devo</p>';
		$term_slug = isset ( $_GET[ 'c' ] ) ? $_GET[ 'c' ] : '';
		$term = $term_slug
			? get_term_by( 'slug', $term_slug, 'links_category' )
			: null
		;
		$term_param = $term
			? 'cus_terms="links_category__' . $term_slug . '"'
			: ''
		;
		$title = $term
			? do_shortcode( '[title]' . $term->name . '[/title]')
			: ''
		;
		$links = do_shortcode( '[blog show_title="yes" title_link="yes" thumbnail="no" excerpt="yes" excerpt_length="999" meta_all="no" meta_author="no" meta_comments="no" meta_date="no" meta_link="no" paging="yes" scrolling="pagination" strip_html="no" blog_grid_columns="2" layout="grid" post_type="avada_links" cus_taxonomy="avada_links__links_category" ' . $term_param . '][/blog]' );


		$terms = $terms = get_terms( array(
			'taxonomy' => 'links_category',
		) );
		$terms_nav = sprintf( '<ul class="fusion-filters clearfix" style="display:block"><li class="fusion-filter fusion-filter-all %s"><a href="%s">All</a></li>'
			, ( $term ? '' : 'fusion-active' )
			, site_url( kds_option( 'links_page_slug' ) )
		);
		foreach ( $terms as $t ) {
			$terms_nav .= sprintf('<li class="fusion-filter %s"><a href="%s">%s</a></li>'			
				, ( $term_slug && $t->slug === $term_slug ? 'fusion-active' : '' )
				, site_url( kds_option( 'links_page_slug' ) . '/?c=' . $t->slug )
				, $t->name
			);  
		}
		$terms_nav .= '</ul>';

		$content = $static_text . $terms_nav . $title . $links;
	}
	return $content;
}

// Links page content / Category
add_filter( 'term_link', 'kds_links_category_link_override', 10, 3 );
function kds_links_category_link_override( $url, $term, $taxonomy ) {
	if ( 'links_category' === $taxonomy ) {
		$url = site_url( kds_option( 'links_page_slug' ) . '/?c=' . $term->slug );
	}
	return $url;
}

// Redirect to link
add_action( 'template_redirect', 'kds_links_CPT_redirect' );
function kds_links_CPT_redirect() {
	global $post;
	$queried_post_type = get_query_var('post_type');
	if ( is_single() && 'avada_links' ==  $queried_post_type ) {
		$url = get_post_meta($post->ID, 'web_link', true);
		if ( ! strpos( $url, '://') ) {
			$url = "http://$url";
		}
		wp_redirect( $url, 301 );
		exit;
	}
}

// Link content
add_filter('the_content', 'kds_link_content');
function kds_link_content( $content ) {
	global $post;
	$post_type = get_post_type( $post );
	switch( $post_type ) {
	case 'avada_links':
		$url = get_post_meta($post->ID, 'web_link', true);
		$url_light = str_replace( array( 'www.', 'http://', 'https://' ), '', $url );
		if ( ! strpos( $url, '://') ) {
			$url = "http://$url";
		}
		$link = sprintf( '<a href="%s">%s</a>', $url, $url_light );
		$content = str_last_replace( '</p>', " &ndash; <em>$link</em></p>", $content );
		break;
	default:
		; 
	}
	return $content;
}
